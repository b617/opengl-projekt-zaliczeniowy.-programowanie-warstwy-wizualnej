#version 330 core

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec2 texCoords;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 index;

uniform mat4 perspective;
uniform mat4 translation;
uniform mat3 rotationX;
uniform mat3 rotationY;
uniform mat3 rotationZ;
uniform mat4 scale;
uniform bool hasTextureCoord;

out vec3 tNormal;
out vec3 FragPos;
out vec2 texCord;

void main()
{
	gl_Position = vec4(vertex*rotationX*rotationY*rotationZ, 1.0f)*scale*translation*perspective;
	FragPos = vertex;
	tNormal = normal*rotationX*rotationY*rotationZ;
	if (hasTextureCoord == true) texCord = texCoords;
	else texCord = vec2(0.0f, 0.0f);
}