#pragma once
#define PI 3.14159265358979323846

#include <GLEW\glew.h>
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
#include <SFML\OpenGL.hpp>
#include <SFML\Graphics\Text.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\System.hpp>
#include <iostream>
#include <fstream>
#include <assimp\scene.h>
#include <assimp\mesh.h>
#include <assimp\Importer.hpp>
#include <assimp\postprocess.h>
#include "Colors.h"
#include "ShaderFiles.h"
#include "Shader.h"
#include "Lights.h"
#include "Model.h"
#include "Game.h"


