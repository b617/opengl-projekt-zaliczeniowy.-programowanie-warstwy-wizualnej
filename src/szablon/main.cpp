#include "includes.h"

void setContext(sf::ContextSettings & _ustawienia, unsigned short _glebia_w_bitach, unsigned short _ilosc_bitow_na_piksel, unsigned short _poziom_antyaliasingu, unsigned short _opengl_ver)
{
	_ustawienia.depthBits = _glebia_w_bitach;
	_ustawienia.stencilBits = _ilosc_bitow_na_piksel;
	_ustawienia.antialiasingLevel = _poziom_antyaliasingu;
	_ustawienia.majorVersion = _opengl_ver;
	_ustawienia.minorVersion = _opengl_ver;
	_ustawienia.attributeFlags = sf::ContextSettings::Core;
}

void glewStart()
{
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "\nGLEW nie zostal zainicjalizowany!!!\n";
		exit(EXIT_FAILURE);
	}
}

int main(int argc, char * argv[])
{
	sf::ContextSettings settings;
	setContext(settings, 32, 8, 4, 4);
	sf::Window window(sf::VideoMode(800, 600), "MemoGame - Wojciech Borkowski & Daniel Janowski", sf::Style::Default, settings);
	window.setVerticalSyncEnabled(true);
	glewStart();

	game MemoGame = game(&window);
	MemoGame.NewGame(); 
	window.close();

	return 0;
}