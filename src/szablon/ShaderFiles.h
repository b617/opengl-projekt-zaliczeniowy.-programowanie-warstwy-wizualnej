#pragma once


struct ShaderFile
{
	/*Nazwy plik�w, dla u�atwienia u�ywania shader�w dla odpowiednich obiekt�w mesh*/
	std::string vertexShaderFile;	
	std::string fragmentShaderFile;
};

namespace Shaders
{
	ShaderFile Default = { "shaders/DefaultShader.vert", "shaders/DefaultShader.frag" };
}