#pragma once
#include "Includes.h"
class game
{
private:
	sf::Window * window;
	const int posLength = 16;
	std::vector<Model*> cards;
	
public:
	game(sf::Window * _window)
	{
		srand(time(NULL));
		window = _window;
		NewGame();
	}

	void NewGame()
	{
		int positions[16]; // do ustalenia pocz�tkowych pozycji kart
		int checked[2][2]; // zmienna do ktorej zapisuje sie wybrane pozycje karty w czasie gry, oraz jej identyfikator
		int cursorPosition; // zmienna opisuj�ca ktora karta jest zaznaczona
		int found; // ilosc znalezionych par

		found = 0;

		for (int i = 0; i < posLength; i++) positions[i] = 0; // zerowanie tablicy
		
		/*ustawianie kart*/
		bool good = false;
		for (int i = 0; i < posLength; i++)
		{
			while (good == false)
			{
				int counter = 0;
				positions[i] = (rand()) % 8 + 1;
				good = true; //zmienna ktora okresla czy czasem nie wylosowano wiecej niz 2 karty tego samego rodzaju

				for (int j = 0; j < i; j++)

				{
					if (positions[j] == positions[i] && counter != 2) counter++;
					if (positions[j] == positions[i] && counter >= 2) good = false;

				}
			}
			good = false;
			std::string sTexture = "";
			switch (positions[i])
			{
			case 1: sTexture = "Karty/kartaWisnia.bmp"; break;
			case 2: sTexture = "Karty/kartaAnanas.bmp"; break;
			case 3: sTexture = "Karty/kartaPor.bmp"; break;
			case 4: sTexture = "Karty/kartaZiemniak.bmp"; break;
			case 5: sTexture = "Karty/kartaTruskawka.bmp"; break;
			case 6: sTexture = "Karty/kartaCebula.bmp"; break;
			case 7: sTexture = "Karty/kartaMarchew.bmp"; break;
			case 8: sTexture = "Karty/kartaGruszka.bmp"; break;
			}
			cards.push_back(new Model("Karty/card.obj", Shaders::Default, GL_TRIANGLES, Colors::Gray, Lights::generateLight(Colors::White, 0.0f, 0.0f, 10.0), sTexture));
			cards[i]->rotateX(90);
			cards[i]->translate(-3.25f + i % 4 * 2.2f, 3.25f - (i / 4)*2.2f, -5.0f);
		}


		sf::Event event;

		checked[0][0] = -1;
		checked[0][1] = -1;
		checked[1][0] = -1;
		checked[1][1] = -1;

		cursorPosition = 0;
		cards[0]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -4.75f);

		/*p�tla gry*/
		while (true)
		{
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			while (window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
				{
					window->close();
					exit(0);
				}
				if (event.type == sf::Event::Resized) glViewport(0, 0, event.size.width, event.size.height);
				if (event.type == sf::Event::KeyPressed)
				{
					if (event.key.code == sf::Keyboard::Down)
					{
						cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -5.0f);
						cursorPosition = (cursorPosition + 4) % 16;
						cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -4.75f);
					}

					if (event.key.code == sf::Keyboard::Up)
					{
						if ((cursorPosition - 4) < 0)
						{
							cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -5.0f);
							cursorPosition = (cursorPosition + 12) % 16;
							cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -4.75f);
						}
						else
						{
							cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -5.0f);
							cursorPosition = (cursorPosition - 4) % 16;
							cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -4.75f);
						}
					}

					if (event.key.code == sf::Keyboard::Left)
					{
						if (cursorPosition - 1 < 0)
						{
							cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -5.0f);
							cursorPosition = 15;
							cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -4.75f);
						}
						else
						{
							cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -5.0f);
							cursorPosition = (cursorPosition - 1) % 16;
							cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -4.75f);
						}
					}

					if (event.key.code == sf::Keyboard::Right)
					{
						cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -5.0f);
						cursorPosition = (cursorPosition + 1) % 16;
						cards[cursorPosition]->translate(-3.25f + cursorPosition % 4 * 2.2f, 3.25f - (cursorPosition / 4)*2.2f, -4.75f);
					}

					if (event.key.code == sf::Keyboard::Space)
					{
						if (positions[cursorPosition] != -1)
						{
							if (checked[0][0] == -1) // gdy nie wybrano jeszcze karty
							{
								renderRotationXAnimation(cursorPosition, 90, 270, 3);
								checked[0][0] = cursorPosition;
								checked[0][1] = positions[cursorPosition];				
							}
							else
							{
								if (checked[0][0] != cursorPosition) //jesli wybrana karta nie jest tak� sam� kart� jak wybrana poprzednio
								{
									renderRotationXAnimation(cursorPosition, 90, 270, 3);
									checked[1][0] = cursorPosition;
									checked[1][1] = positions[cursorPosition];

										if (checked[0][1] == checked[1][1]) // gdy wybrane karty s� takie same
										{
											positions[checked[0][0]] = -1;
											positions[checked[1][0]] = -1;

											checked[0][0] = -1;
											checked[0][1] = -1;
											checked[1][0] = -1;
											checked[1][1] = -1;
											found++;
										}
										else // gdy wybrane karty nie s� takie same
										{
											renderRotationXAnimation(checked[0][0], 270, 90, 6);
											renderRotationXAnimation(checked[1][0], 270, 90, 6);
											checked[0][0] = -1;
											checked[0][1] = -1;
											checked[1][0] = -1;
											checked[1][1] = -1;
										}
								}
							}
						}
					}
				}
			}

			renderActualGame();
			window->display();

			/*zatrzymanie gry przy odkryciu wszystkich kart*/
			if (found == 8)
			{
				while (true)
				{
					while (window->pollEvent(event))
					{
						if (event.type == sf::Event::Closed)
						{
							window->close();
							exit(0);
						}
						if (event.type == sf::Event::Resized) glViewport(0, 0, event.size.width, event.size.height);
						if (event.type == sf::Event::KeyPressed)
						{
							window->close();
							exit(0);
						}
					}
				}
			}
		}
		
		
	}

	/*narysuj scene*/
	void renderActualGame()
	{
		for (int i = 0; i < cards.size(); i++)
		{
			cards[i]->render();	
		}
	}

	/*animacja obrot�w kart*/
	void renderRotationXAnimation(int cursorPos, int startAngle, int targetAngle, int step)
	{
		int theAngle = startAngle;
		while (theAngle != targetAngle)
		{
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			theAngle = (theAngle + step)%360;
			cards[cursorPos]->rotateX(theAngle);
			renderActualGame();
			window->display();
		}
	}
};