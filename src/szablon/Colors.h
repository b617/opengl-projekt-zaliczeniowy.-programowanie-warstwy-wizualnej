#pragma once

namespace Colors
{
	GLfloat Red[] = { 1.0f, 0.0f, 0.0f };
	GLfloat Green[] = { 0.0f, 1.0f, 0.0f };
	GLfloat Blue[] = { 0.0f, 0.0f, 1.0f };
	GLfloat Cyan[] = { 0.0f, 1.0f, 1.0f };
	GLfloat Black[] = { 0.0f, 0.0f, 0.0f };
	GLfloat Yellow[] = { 1.0f, 1.0f, 0.0f };
	GLfloat Brown[] = { (150.0f / 256.0f), (75.0f / 256.0f), 0.0f };
	GLfloat Orange[] = { (1.0f, 165.0f / 256.0f), 0.0f };
	GLfloat Purple[] = { 1.0f, 0.0f, 1.0f };
	GLfloat Gray[] = { 0.8f, 0.8f, 0.8f };
	GLfloat White[] = { 1.0f, 1.0f, 1.0f };
}
