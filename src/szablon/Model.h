#pragma once
#include "Includes.h"

class Model
{
public:
	struct MeshEntry
	{
		static enum BUFFERS { VERTEX_BUFFER, TEXCOORD_BUFFER, NORMAL_BUFFER, INDEX_BUFFER};
		GLuint vao;
		GLuint vbo[4];
		GLuint drawMode;

		shader Shader;
		unsigned int elementCount;
		MeshEntry(aiMesh *mesh, GLuint drawMode, shader _shader);
		~MeshEntry();
		void render();
	};

	sf::Image image;
	GLuint texture;
	shader tShader;
	std::vector<MeshEntry*> meshEntries;

	Model(const char *filename, ShaderFile _shaderfile, GLuint drawMode, GLfloat color[3], Lights::Light theLight, std::string texturePath);
	~Model(void);
	
	void render();
	void translate(GLfloat x, GLfloat y, GLfloat z);
	void scale(GLfloat x, GLfloat y, GLfloat z);
	void rotateX(GLfloat _angle);
	void rotateY(GLfloat _angle);
	void rotateZ(GLfloat _angle);
	
};

Model::MeshEntry::MeshEntry(aiMesh *mesh, GLuint _drawMode, shader _shader)
{
	vbo[VERTEX_BUFFER] = NULL;
	vbo[TEXCOORD_BUFFER] = NULL;
	vbo[NORMAL_BUFFER] = NULL;
	vbo[INDEX_BUFFER] = NULL;
	drawMode = _drawMode;
	Shader = _shader;

	glm::mat4 persp = glm::perspective(45.0f, 1.0f, 1.0f, -15.0f);
	glUseProgram(Shader.getProgram());
	glUniformMatrix4fv(Shader.getUniformLocation("perspective"), 1, GL_FALSE, &persp[0][0]);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	elementCount = mesh->mNumFaces * 3;

	if (mesh->HasPositions()) 
	{
		float *vertices = new float[mesh->mNumVertices * 3];
		for (int i = 0; i < mesh->mNumVertices; ++i) 
		{
			vertices[i * 3] = mesh->mVertices[i].x;
			vertices[i * 3 + 1] = mesh->mVertices[i].y;
			vertices[i * 3 + 2] = mesh->mVertices[i].z;
		}

		glGenBuffers(1, &vbo[VERTEX_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[VERTEX_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), vertices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		delete[] vertices;
	}


	if (mesh->HasTextureCoords(0)) 
	{

		GLuint texCoordLoc = Shader.getUniformLocation("hasTextureCoord");
		glUniform1f(texCoordLoc, true);

		float *texCoords = new float[mesh->mNumVertices * 2];
		for (int i = 0; i < mesh->mNumVertices; ++i) 
		{
			texCoords[i * 2] = mesh->mTextureCoords[0][i].x;
			texCoords[i * 2 + 1] = mesh->mTextureCoords[0][i].y;
		}

		glGenBuffers(1, &vbo[TEXCOORD_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[TEXCOORD_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, 2 * mesh->mNumVertices * sizeof(GLfloat), texCoords, GL_STATIC_DRAW);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(1);

		delete[] texCoords;
	}
	else
	{
		GLuint texCoordLoc = Shader.getUniformLocation("hasTextureCoord");
		glUniform1f(texCoordLoc, false);
		std::cout << "\nUwaga: Brak wspolrzednych tekstur, rysowanie bez tekstur";
	}

	if (mesh->HasNormals()) 
	{
		float *normals = new float[mesh->mNumVertices * 3];
		for (int i = 0; i < mesh->mNumVertices; ++i) 
		{
			normals[i * 3] = mesh->mNormals[i].x;
			normals[i * 3 + 1] = mesh->mNormals[i].y;
			normals[i * 3 + 2] = mesh->mNormals[i].z;
		}

		glGenBuffers(1, &vbo[NORMAL_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[NORMAL_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), normals, GL_STATIC_DRAW);

		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(2);

		delete[] normals;
	}
	else
	{
		std::cout << "\nUwaga! Brak normali";
	}


	if (mesh->HasFaces()) 
	{
		unsigned int *indices = new unsigned int[mesh->mNumFaces * 3];
		for (int i = 0; i < mesh->mNumFaces; ++i) 
		{
			indices[i * 3] = mesh->mFaces[i].mIndices[0];
			indices[i * 3 + 1] = mesh->mFaces[i].mIndices[1];
			indices[i * 3 + 2] = mesh->mFaces[i].mIndices[2];
		}

		glGenBuffers(1, & vbo[INDEX_BUFFER]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[INDEX_BUFFER]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * mesh->mNumFaces * sizeof(GLuint), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(3);

		delete[] indices;
	}


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

Model::MeshEntry::~MeshEntry() 
{
	if (vbo[VERTEX_BUFFER]) 
	{
		glDeleteBuffers(1, &vbo[VERTEX_BUFFER]);
	}

	if (vbo[TEXCOORD_BUFFER])
	{
		glDeleteBuffers(1, &vbo[TEXCOORD_BUFFER]);
	}

	if (vbo[NORMAL_BUFFER]) 
	{
		glDeleteBuffers(1, &vbo[NORMAL_BUFFER]);
	}

	if (vbo[INDEX_BUFFER]) 
	{
		glDeleteBuffers(1, &vbo[INDEX_BUFFER]);
	}

	glDeleteVertexArrays(1, & vao);
}

void Model::MeshEntry::render() 
{

	glUseProgram(Shader.getProgram());
	glBindVertexArray(vao);
	glDrawElements(drawMode, elementCount, GL_UNSIGNED_INT, NULL);
	glBindVertexArray(0);
    glUseProgram(0);

}

Model::Model(const char *filename, ShaderFile _shaderfile, GLuint _drawMode, GLfloat color[3], Lights::Light theLight, std::string texturePath)
{
	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(filename, NULL);
	if (!scene) printf("Unable to load mesh: %s\n", importer.GetErrorString());

	tShader.setShaders(_shaderfile.vertexShaderFile, _shaderfile.fragmentShaderFile);
	this->rotateX(0.0f);
	this->rotateY(0.0f);
	this->rotateZ(0.0f);
	this->scale(1.0f, 1.0f, 1.0f);
	this->translate(0.0f, 0.0f, 0.0f);

	for (int i = 0; i < scene->mNumMeshes; ++i) 
	{
		meshEntries.push_back(new Model::MeshEntry(scene->mMeshes[i],_drawMode,  tShader));
	}

	

	
	if (!image.loadFromFile(texturePath))
	{
		std::cout << "Nie wczytano tekstury!!!";
		system("PAUSE");
		std::exit(1);
	}

	GLuint texture;
	glGenTextures(1, &texture);
	

	glUseProgram(tShader.getProgram());
	glUniform3f(tShader.getUniformLocation("objectColor"), color[0], color[1], color[2]);
	glUniform3f(tShader.getUniformLocation("lightPos"), theLight.position[0], theLight.position[1], theLight.position[2]);
	glUniform3f(tShader.getUniformLocation("lightColor"), theLight.color[0], theLight.color[1], theLight.color[2]);
	glUseProgram(0);

}

Model::~Model(void)
{
	for (int i = 0; i < meshEntries.size(); ++i) 
	{
		delete meshEntries.at(i);
	}
	meshEntries.clear();
}

/*funkcja rysuj�ca na scenie obiekt*/
void Model::render() 
{
	for (int i = 0; i < meshEntries.size(); ++i) 
	{
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.getSize().x, image.getSize().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		meshEntries.at(i)->render();
	}
}

/*funkcja przesuni�cia obiektu*/
void Model::translate(GLfloat x, GLfloat y, GLfloat z)
{
	glm::mat4 transMatrix =
	{
		1,0,0,x,
		0,1,0,y,
		0,0,1,z,
		0,0,0,1
	};

	GLuint T = glGetUniformLocation(tShader.getProgram(), "translation");
	glUseProgram(tShader.getProgram());
	glUniformMatrix4fv(T, 1, GL_FALSE, &transMatrix[0][0]);
	glUseProgram(0);
}

/*funkcja rotuj�ca w okol osi X*/
void Model::rotateX(GLfloat _angle)
{
	GLfloat angle = _angle * PI / 180;
	glm::mat3 rotMatrix =
	{
		1,0,0,
		0,glm::cos(angle),-glm::sin(angle),
		0,glm::sin(angle),glm::cos(angle)
	};

	GLuint R = glGetUniformLocation(tShader.getProgram(), "rotationX");
	glUseProgram(tShader.getProgram());
	glUniformMatrix3fv(R, 1, GL_FALSE, &rotMatrix[0][0]);
	glUseProgram(0);
}

/*funkcja rotuj�ca w okol osi Y*/
void Model::rotateY(GLfloat _angle)
{
	GLfloat angle = _angle * PI / 180;
	glm::mat3 rotMatrix =
	{
		glm::cos(angle),0,glm::sin(angle),
		0,1,0,
		-glm::sin(angle),0, glm::cos(angle),
	};

	GLuint R = glGetUniformLocation(tShader.getProgram(), "rotationY");
	glUseProgram(tShader.getProgram());
	glUniformMatrix3fv(R, 1, GL_FALSE, &rotMatrix[0][0]);
	glUseProgram(0);
}

/*funkcja rotuj�ca w okol osi Z*/
void Model::rotateZ(GLfloat _angle)
{
	GLfloat angle = _angle * PI / 180;

	glm::mat3 rotMatrix =
	{
		glm::cos(angle),-glm::sin(angle),0,
		glm::sin(angle),glm::cos(angle),0,
		0,0,1
	};

	GLuint R = glGetUniformLocation(tShader.getProgram(), "rotationZ");
	glUseProgram(tShader.getProgram());
	glUniformMatrix3fv(R, 1, GL_FALSE, &rotMatrix[0][0]);
	glUseProgram(0);
}

/*funkcja skaluj�ca*/
void Model::scale(GLfloat x, GLfloat y, GLfloat z)
{
	glm::mat4 scaleMatrix =
	{
		x,0,0,0,
		0,y,0,0,
		0,0,z,0,
		0,0,0,1
	};
	glUseProgram(tShader.getProgram());
	glUniformMatrix4fv(tShader.getUniformLocation("scale"), 1, GL_FALSE, &scaleMatrix[0][0]);
	glUseProgram(0);
}