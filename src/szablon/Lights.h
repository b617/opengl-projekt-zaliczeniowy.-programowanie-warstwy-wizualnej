#pragma once

namespace Lights
{	
	struct Light
	{
		GLfloat color[3];
		GLfloat position[3];
	};

	Light generateLight(GLfloat color[3], GLfloat posX, GLfloat posY, GLfloat posZ)
	{
		Light theLight;
		theLight.color[0] = color[0];
		theLight.color[1] = color[1];
		theLight.color[2] = color[2];
		theLight.position[0] = posX;
		theLight.position[1] = posY;
		theLight.position[2] = posZ;
		return theLight;
	}
}